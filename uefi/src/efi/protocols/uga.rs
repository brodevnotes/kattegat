use super::{BltOperation, BltPixel};

#[repr(C)]
pub struct UniversalGraphicsProtocol {
    get_mode: extern "efiapi" fn(
        &UniversalGraphicsProtocol,
        horizontal_resolution: &mut u32,
        vertical_resolution: &mut u32,
        color_depth: &mut u32,
        refresh_rate: &mut u32,
    ) -> usize,
    set_mode: extern "efiapi" fn(
        &UniversalGraphicsProtocol,
        horizontal_resolution: u32,
        vertical_resolution: u32,
        color_depth: u32,
        refresh_rate: u32,
    ) -> usize,
    blt: extern "efiapi" fn(
        &UniversalGraphicsProtocol,
        blt_buffer: *mut BltPixel,
        blt_operation: u32,
        source_x: usize,
        source_y: usize,
        destination_x: usize,
        destination_y: usize,
        width: usize,
        height: usize,
        delta: Option<usize>,
    ) -> usize,
}

impl UniversalGraphicsProtocol {
    pub fn set_mode(
        &mut self,
        resolution: (u32, u32),
        color_depth: u32,
        refresh_rate: u32,
    ) -> usize {
        let status = (self.set_mode)(self, resolution.0, resolution.1, color_depth, refresh_rate);

        status
    }

    pub fn blt(&mut self, operation: BltOperation) -> usize {
        match operation {
            BltOperation::VideoFill {
                color,
                destination: (dest_x, dest_y),
                dimensions: (width, height),
            } => (self.blt)(
                self,
                &color as *const _ as *mut _,
                0,
                0,
                0,
                dest_x,
                dest_y,
                width,
                height,
                Some(0),
            ),
            _ => 0,
        }
    }

    pub fn get_mode(&self) -> (u32, u32, u32, u32) {
        let mut horizontal_resolution = 0;
        let mut vertical_resolution = 0;
        let mut color_depth = 0;
        let mut refresh_rate = 0;

        (self.get_mode)(
            self,
            &mut horizontal_resolution,
            &mut vertical_resolution,
            &mut color_depth,
            &mut refresh_rate,
        );

        (
            horizontal_resolution,
            vertical_resolution,
            color_depth,
            refresh_rate,
        )
    }
}
