use core::{ffi::c_void, ptr};

use super::{BltOperation, BltPixel, PixelFormat};

#[repr(C)]
pub struct GraphicsOutputProtocol<'open_protocol> {
    query_mode: extern "efiapi" fn(
        &GraphicsOutputProtocol,
        mode_number: u32,
        size_of_info: &mut usize,
        info: &mut *const ModeInfo,
    ) -> usize,
    set_mode: extern "efiapi" fn(&mut GraphicsOutputProtocol, mode_number: u32) -> usize,
    blt: extern "efiapi" fn(
        this: &mut GraphicsOutputProtocol,
        blt_buffer: *mut BltPixel,
        blt_operation: u32,
        source_x: usize,
        source_y: usize,
        destination_x: usize,
        destination_y: usize,
        width: usize,
        height: usize,
        delta: Option<usize>,
    ) -> usize,
    mode: &'open_protocol Mode<'open_protocol>,
}

impl<'open_protocol> GraphicsOutputProtocol<'open_protocol> {
    pub fn query_mode(&self, index: u32) -> Option<GraphicsMode> {
        let mut info_size = 0;
        let mut info = ptr::null();
        let status = (self.query_mode)(self, index, &mut info_size, &mut info);
        if status == 0 {
            let info = unsafe { *info };
            return Some(GraphicsMode { index, info });
        } else {
            return None;
        }
    }

    pub fn modes(&'_ self) -> impl ExactSizeIterator<Item = GraphicsMode> + '_ {
        GraphicsModeIterator {
            gop: self,
            current_index: 0,
            max_mode: self.mode.max_mode,
        }
    }

    pub fn set_mode(&mut self, mode: &GraphicsMode) -> usize {
        (self.set_mode)(self, mode.index)
    }

    pub fn blt(&mut self, operation: BltOperation) -> usize {
        match operation {
            BltOperation::VideoFill {
                color,
                destination: (dest_x, dest_y),
                dimensions: (width, height),
            } => (self.blt)(
                self,
                &color as *const _ as *mut _,
                0,
                0,
                0,
                dest_x,
                dest_y,
                width,
                height,
                Some(0),
            ),
            _ => 0,
        }
    }

    pub fn mode_info(&self) -> ModeInfo {
        *self.mode.info
    }

    pub fn frame_buffer(&mut self) -> (*mut c_void, usize) {
        (
            self.mode.frame_buffer_base as *mut c_void,
            self.mode.frame_buffer_size,
        )
    }
}

pub struct GraphicsMode {
    index: u32,
    info: ModeInfo,
}

impl GraphicsMode {
    pub fn info(&self) -> ModeInfo {
        self.info
    }
}
struct GraphicsModeIterator<'a> {
    gop: &'a GraphicsOutputProtocol<'a>,
    current_index: u32,
    max_mode: u32,
}

impl<'a> Iterator for GraphicsModeIterator<'a> {
    type Item = GraphicsMode;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current_index < self.max_mode {
            let mode = self.gop.query_mode(self.current_index);
            self.current_index += 1;
            return mode;
        }
        None
    }
}

impl ExactSizeIterator for GraphicsModeIterator<'_> {}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct ModeInfo {
    version: u32,
    horizontal_resolution: u32,
    vertical_resolution: u32,
    pixel_format: PixelFormat,
    pixel_information: PixelMask,
    pixels_per_scan_line: u32,
}

impl ModeInfo {
    pub fn resolution(&self) -> (usize, usize) {
        (
            self.horizontal_resolution as usize,
            self.vertical_resolution as usize,
        )
    }

    pub fn pixel_format(&self) -> PixelFormat {
        self.pixel_format
    }

    pub fn stride(&self) -> usize {
        self.pixels_per_scan_line as usize
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PixelMask {
    red: u32,
    green: u32,
    blue: u32,
    _reserved: u32,
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct Mode<'open_protocol> {
    max_mode: u32,
    mode: u32,
    info: &'open_protocol ModeInfo,
    size_of_info: usize,
    frame_buffer_base: usize,
    frame_buffer_size: usize,
}
