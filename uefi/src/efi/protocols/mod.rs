use self::{
    con_control::ConsoleControlProtocol, gop::GraphicsOutputProtocol,
    uga::UniversalGraphicsProtocol,
};

use super::{BootServices, Guid, Handle, OpenProtocolAttribute};

pub mod con_control;
pub mod con_out;
pub mod gop;
pub mod uga;

pub const EFI_GRAPHICS_OUTPUT_PROTOCOL: Guid = Guid {
    data1: 0x9042a9de,
    data2: 0x23dc,
    data3: 0x4a38,
    data4: [0x96, 0xfb, 0x7a, 0xde, 0xd0, 0x80, 0x51, 0x6a],
};

const EFI_CONSOLE_CONTROL_GUID: Guid = Guid {
    data1: 0xf42f7782,
    data2: 0x12e,
    data3: 0x4c12,
    data4: [0x99, 0x56, 0x49, 0xf9, 0x43, 0x4, 0xf7, 0x21],
};

const EFI_UGA_DRAW_PROTOCOL: Guid = Guid {
    data1: 0x982c298b,
    data2: 0xf4fa,
    data3: 0x41cb,
    data4: [0xb8, 0x38, 0x77, 0xaa, 0x68, 0x8f, 0xb8, 0x39],
};

#[derive(Copy, Clone, Debug)]
#[repr(u32)]
pub enum PixelFormat {
    RedGreenBlueReserved8BitPerColor = 0,
    BlueGreenRedReserved8BitPerColor,
    PixelBitMask,
    PixelBltOnly,
    PixelFormatMax,
}

#[repr(C)]
pub struct BltPixel {
    blue: u8,
    green: u8,
    red: u8,
    _reserved: u8,
}

impl BltPixel {
    pub fn new(red: u8, green: u8, blue: u8) -> Self {
        Self {
            red,
            green,
            blue,
            _reserved: 0,
        }
    }
}

pub enum BltOperation {
    VideoFill {
        color: BltPixel,
        destination: (usize, usize),
        dimensions: (usize, usize),
    },
    _VideoToBltBuffer,
    _BufferToVideo,
    _VideoToVideo,
    _GOPBltOperationMax,
}

pub fn get_gop(
    bs: &BootServices,
    console_out_handle: Handle,
) -> Result<&mut GraphicsOutputProtocol, usize> {
    let found = bs.open_protocol(
        console_out_handle,
        EFI_GRAPHICS_OUTPUT_PROTOCOL,
        console_out_handle,
        None,
        OpenProtocolAttribute::GetProtocol,
    )?;
    let interface = found.cast::<GraphicsOutputProtocol>();
    let gop = unsafe { &mut *interface };
    return Ok(gop);
}

pub fn get_console_control(
    bs: &BootServices,
    console_out_handle: Handle,
) -> Result<&mut ConsoleControlProtocol, usize> {
    let found = bs.open_protocol(
        console_out_handle,
        EFI_CONSOLE_CONTROL_GUID,
        console_out_handle,
        None,
        OpenProtocolAttribute::GetProtocol,
    )?;
    let interface = found.cast::<ConsoleControlProtocol>();
    let uga = unsafe { &mut *interface };
    return Ok(uga);
}

pub fn get_uga(
    bs: &BootServices,
    console_out_handle: Handle,
) -> Result<&mut UniversalGraphicsProtocol, usize> {
    let found = bs.open_protocol(
        console_out_handle,
        EFI_UGA_DRAW_PROTOCOL,
        console_out_handle,
        None,
        OpenProtocolAttribute::GetProtocol,
    )?;
    let interface = found.cast::<UniversalGraphicsProtocol>();
    let uga = unsafe { &mut *interface };
    return Ok(uga);
}
