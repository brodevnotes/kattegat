#[repr(C)]
pub struct ConsoleControlProtocol {
    get_mode: extern "efiapi" fn(
        &ConsoleControlProtocol,
        mode: &mut ScreenMode,
        uga_exists: &mut bool,
        std_in_locked: &mut bool,
    ) -> usize,
    set_mode: extern "efiapi" fn(&ConsoleControlProtocol, mode: ScreenMode) -> usize,
    lock_std_in: extern "efiapi" fn(&ConsoleControlProtocol, password: &mut u16) -> usize,
}

#[derive(Debug)]
pub enum ScreenMode {
    Text,
    Graphics,
    MaxValue,
}

impl ConsoleControlProtocol {
    pub fn get_mode(&self) -> (ScreenMode, bool, bool) {
        let mut screen_mode = ScreenMode::Text;
        let mut uga_exists = false;
        let mut std_in_locked = false;

        (self.get_mode)(self, &mut screen_mode, &mut uga_exists, &mut std_in_locked);

        (screen_mode, uga_exists, std_in_locked)
    }
}
