use crate::efi::Handle;

#[repr(C)]
pub struct ConsoleOut {
    reset: extern "efiapi" fn(this: &ConsoleOut, extended: bool) -> usize,
    output_string: unsafe extern "efiapi" fn(this: &ConsoleOut, string: *const u16) -> usize,
    test_string: Handle,
    query_mode: Handle,
    set_mode: Handle,
    set_attribute: Handle,
    clear_streen: Handle,
    set_cursor_position: Handle,
    enable_cursor: Handle,
    output_mode: Handle,
}

impl ConsoleOut {
    pub fn reset(&mut self, extended: bool) -> usize {
        let status = (self.reset)(self, extended);
        status
    }

    pub fn output_string(&mut self, string: &str) -> usize {
        let mut buf = [0u16; 129];
        for (i, ch) in string.encode_utf16().enumerate() {
            buf[i] = ch;
        }

        let status = unsafe { (self.output_string)(self, &buf as *const [u16] as *const u16) };

        status
    }
}

impl core::fmt::Write for ConsoleOut {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        self.output_string(s);
        Ok(())
    }
}
