pub mod protocols;

use core::{ffi::c_void, ops::Deref, ptr, ptr::NonNull, slice};

use self::protocols::con_out::ConsoleOut;

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Handle(NonNull<c_void>);

#[derive(Clone, Copy)]
#[repr(C)]
pub struct EFIHeader {
    pub signature: u64,
    pub revision: u32,
    pub header_size: u32,
    pub crc32: u32,
    _reserved: u32,
}

#[repr(C)]
pub struct SystemTable {
    hdr: EFIHeader,
    firmware_vendor: *const u16,
    firmware_revision: u32,
    console_in_handle: Handle,
    console_in: Handle,
    console_out_handle: Handle,
    console_out: *mut ConsoleOut,
    standard_error_handle: Handle,
    std_err: Handle,
    runtime_services: Handle,
    boot_services: *const BootServices,
    number_of_table_entries: usize,
    configuration_table: *const ConfigurationTable,
}

impl SystemTable {
    pub fn header(&self) -> EFIHeader {
        self.hdr
    }

    pub fn console_out_handle(&self) -> Handle {
        self.console_out_handle
    }

    pub fn console_out(&mut self) -> &mut ConsoleOut {
        unsafe { &mut *self.console_out.cast() }
    }

    pub fn boot_services(&self) -> &BootServices {
        unsafe { &*self.boot_services }
    }

    pub fn acpi_tables(&self) -> &[ConfigurationTable] {
        unsafe { slice::from_raw_parts(self.configuration_table, self.number_of_table_entries) }
    }
}

#[repr(C)]
pub struct BootServices {
    hdr: EFIHeader,

    // Task Priority Services
    raise_tpl: Handle,
    restore_tpl: Handle,

    // Memory Services
    allocate_pages: Handle,
    free_pages: Handle,
    get_memory_map: extern "efiapi" fn(
        memory_map_size: &mut usize,
        memory_map: *mut MemoryDescriptor,
        map_key: &mut usize,
        descriptor_size: &mut usize,
        descriptor_version: &mut u32,
    ) -> usize,
    allocate_pool:
        extern "efiapi" fn(pool_type: MemoryType, size: usize, buffer: &mut *mut u8) -> usize,
    free_pool: unsafe extern "efiapi" fn(buffer: *mut u8) -> usize,

    // Event & Time Services
    create_event: Handle,
    set_timer: Handle,
    wait_for_event: Handle,
    signal_event: Handle,
    close_event: Handle,
    check_event: Handle,

    // Protocol Handler Services
    install_protocol_interface: Handle,
    reinstall_protocol_interface: Handle,
    uninstall_protocol_interface: Handle,
    handle_protocol: Handle,
    _reserved: Handle,
    register_protocol_notify: Handle,
    locate_handle: Handle,
    locate_device_path: Handle,
    install_configuration_table: Handle,

    // Image Services
    load_image: Handle,
    start_image: Handle,
    exit: Handle,
    unload_image: Handle,
    exit_boot_services: extern "efiapi" fn(image_handle: Handle, map_key: usize) -> usize,

    // Misc Services
    get_next_monotonic_count: Handle,
    stall: Handle,
    set_watchdog_timer: Handle,

    // DriverSupport Services
    connect_controller: Handle,
    disconnect_controller: Handle,

    // Adds elements to the list of agents consuming a protocol interface.
    open_protocol: extern "efiapi" fn(
        handle: Handle,
        protocol: &Guid,
        interface: &mut *mut c_void,
        agent_handle: Handle,
        controller_handle: Option<Handle>,
        attributes: u32,
    ) -> usize,
    close_protocol: Handle,
    open_protocol_information: Handle,

    // Library Services
    protocols_per_handle: extern "efiapi" fn(
        handle: Handle,
        protocol_buffer: &mut *mut *const Guid,
        protocol_buffer_count: &mut usize,
    ) -> usize,

    locate_handle_buffer: extern "efiapi" fn(
        search_type: i32,
        prototype: *const Guid,
        search_key: *const c_void,
        no_handles: *mut usize,
        buffer: *mut *mut Handle,
    ) -> usize,

    locate_protocol: Handle,
    install_multiple_protocol_interfaces: Handle,
    uninstall_multiple_protocol_interfaces: Handle,

    // 32-bit CRC Services
    calculate_crc32: Handle,

    // Misc Services
    copy_mem: Handle,
    set_mem: Handle,
    create_event_ex: Handle,
}

impl BootServices {
    pub fn open_protocol(
        &self,
        handle: Handle,
        protocol: Guid,
        agent: Handle,
        controller: Option<Handle>,
        attributes: OpenProtocolAttribute,
    ) -> Result<*mut c_void, usize> {
        let mut interface = ptr::null_mut();
        let status = (self.open_protocol)(
            handle,
            &protocol,
            &mut interface,
            agent,
            controller,
            attributes as u32,
        );

        if status != 0 {
            return Err(status);
        }

        Ok(interface)
    }

    pub fn memory_map_size(&self) -> Result<(usize, usize), usize> {
        let mut map_size = 0;
        let mut map_key = 0;
        let mut entry_size = 0;
        let mut entry_version = 0;

        let status = (self.get_memory_map)(
            &mut map_size,
            ptr::null_mut(),
            &mut map_key,
            &mut entry_size,
            &mut entry_version,
        );

        if status != 0x8000000000000005 {
            return Err(status);
        }

        Ok((map_size, entry_size))
    }

    pub fn allocate_pool(&self, mem_ty: MemoryType, size: usize) -> Result<*mut u8, usize> {
        let mut buffer = ptr::null_mut();

        let status = (self.allocate_pool)(mem_ty, size, &mut buffer);

        if status != 0 {
            return Err(status);
        }

        Ok(buffer)
    }

    pub unsafe fn free_pool(&self, address: *mut u8) -> usize {
        unsafe { (self.free_pool)(address) }
    }

    pub fn get_memory_map<'buf>(
        &self,
        buffer: &'buf mut [u8],
    ) -> Result<
        (
            usize,
            impl ExactSizeIterator<Item = &'buf MemoryDescriptor> + Clone,
        ),
        usize,
    > {
        let mut map_size = buffer.len();

        let map_buffer = buffer.as_mut_ptr().cast::<MemoryDescriptor>();
        let mut map_key = 0;
        let mut entry_size = 0;
        let mut entry_version = 0;

        let status = (self.get_memory_map)(
            &mut map_size,
            map_buffer,
            &mut map_key,
            &mut entry_size,
            &mut entry_version,
        );

        if status != 0 {
            return Err(status);
        }

        let len = map_size / entry_size;

        Ok((
            map_key,
            MemoryMapIter {
                buffer,
                entry_size,
                index: 0,
                len,
            },
        ))
    }

    pub fn exit_boot_services(&self, image: Handle, memory_map_key: usize) -> Result<(), usize> {
        let status = (self.exit_boot_services)(image, memory_map_key);

        if status != 0 {
            return Err(status);
        }

        Ok(())
    }

    pub fn protocols_per_handle(&self, handle: Handle) -> Result<&[*const Guid], usize> {
        let mut buffer = ptr::null_mut();
        let mut buffer_size = 0;

        let status = (self.protocols_per_handle)(handle, &mut buffer, &mut buffer_size);

        if status != 0 {
            return Err(status);
        }

        if buffer.is_null() {
            return Err(1);
        }

        let protocols = unsafe { slice::from_raw_parts(buffer, buffer_size) };

        if protocols.iter().any(|ptr| ptr.is_null()) {
            return Err(1);
        }

        Ok(protocols)
    }

    pub fn locate_handle_buffer(&self, search_ty: SearchType) -> Result<HandleBuffer, usize> {
        let mut num_handles: usize = 0;
        let mut buffer: *mut Handle = ptr::null_mut();

        // Obtain the needed data from the parameters.
        let (ty, guid, key) = search_ty.get_handle_params();
        let status = (self.locate_handle_buffer)(ty, guid, key, &mut num_handles, &mut buffer);

        if status != 0 {
            return Err(status);
        }

        Ok(HandleBuffer {
            boot_services: self,
            count: num_handles,
            buffer: buffer.cast(),
        })
    }
}

pub enum SearchType<'guid> {
    AllHandles,
    ByProtocol(&'guid Guid),
    ByRegisterNotify(NonNull<c_void>),
}

impl<'guid> SearchType<'guid> {
    pub fn get_handle_params(&self) -> (i32, *const Guid, *const c_void) {
        match self {
            Self::AllHandles => (0, ptr::null(), ptr::null()),
            Self::ByRegisterNotify(registration) => {
                (1, ptr::null(), registration.as_ptr().cast_const())
            }
            Self::ByProtocol(guid) => (2, *guid as *const _, ptr::null()),
        }
    }
}

pub struct HandleBuffer<'a> {
    boot_services: &'a BootServices,
    count: usize,
    buffer: *mut Handle,
}

impl<'a> Drop for HandleBuffer<'a> {
    fn drop(&mut self) {
        let _ = unsafe { self.boot_services.free_pool(self.buffer.cast::<u8>()) };
    }
}

impl<'a> HandleBuffer<'a> {
    pub fn handles(&self) -> &[Handle] {
        unsafe { slice::from_raw_parts(self.buffer, self.count) }
    }
}

#[derive(Clone)]
struct MemoryMapIter<'buf> {
    buffer: &'buf [u8],
    entry_size: usize,
    index: usize,
    len: usize,
}

impl<'buf> Iterator for MemoryMapIter<'buf> {
    type Item = &'buf MemoryDescriptor;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index < self.len {
            let ptr = self.buffer.as_ptr() as usize + self.entry_size * self.index;

            self.index += 1;

            let descriptor = unsafe { &*(ptr as *const MemoryDescriptor) };

            Some(descriptor)
        } else {
            None
        }
    }
}

impl ExactSizeIterator for MemoryMapIter<'_> {}

#[repr(u32)]
pub enum OpenProtocolAttribute {
    ByHandleProtocol = 0x01,
    GetProtocol = 0x02,
    TestProtocol = 0x04,
    ByChildController = 0x08,
    ByDriver = 0x10,
    ByExclusive = 0x20,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
#[repr(C)]
pub struct Guid {
    data1: u32,
    data2: u16,
    data3: u16,
    data4: [u8; 8],
}

#[repr(C)]
pub struct ConfigurationTable {
    pub guid: Guid,
    pub address: *const c_void,
}

/// Entry pointing to the old ACPI 1 RSDP.
pub const ACPI_GUID: Guid = Guid {
    data1: 0xeb9d2d30,
    data2: 0x2d88,
    data3: 0x11d3,
    data4: [0x9a, 0x16, 0x00, 0x90, 0x27, 0x3f, 0xc1, 0x4d],
};

/// Entry pointing to the ACPI 2 RSDP.
pub const ACPI2_GUID: Guid = Guid {
    data1: 0x8868e871,
    data2: 0xe4f1,
    data3: 0x11d3,
    data4: [0xbc, 0x22, 0x00, 0x80, 0xc7, 0x3c, 0x88, 0x81],
};

#[derive(Debug)]
pub enum RSDPType {
    Acpi1,
    Acpi2,
    Other,
}

pub fn find_rsdp(acpi_tables: &[ConfigurationTable]) -> (RSDPType, *const c_void) {
    let mut rsdp_address = None;
    for table in acpi_tables {
        match table.guid {
            ACPI_GUID => {
                // fallback ACPI < 2.0
                rsdp_address = Some((RSDPType::Acpi1, table.address));
            }
            ACPI2_GUID => {
                // if ACPI >= 2.0 we have what we want
                return (RSDPType::Acpi2, table.address);
            }
            _ => {
                // only BIOS available, not expected on a modern UEFI
                if rsdp_address.is_none() {
                    rsdp_address = Some((RSDPType::Other, table.address));
                }
            }
        }
    }

    rsdp_address.expect("ACPI RSDP not found")
}

#[repr(C)]
#[derive(Debug)]
pub struct MemoryDescriptor {
    pub memory_type: MemoryType,
    _padding: u32,
    pub physical_start: u64,
    pub virtual_start: u64,
    pub number_of_pages: u64,
    pub memory_attribute: u64,
}

#[repr(u32)]
#[derive(Debug)]
pub enum MemoryType {
    _Reserved = 0,
    // The code portions of a loaded UEFI application (Free after exit)
    LoaderCode,
    // The data portions of a loaded UEFI application and the default data allocation type used by a UEFI application to allocate pool memory (Free after exit)
    LoaderData,
    // The code portions of a loaded UEFI Boot Service Driver (Free after exit)
    BootServicesCode,
    // The data portions of a loaded UEFI Boot Serve Driver, and the default data allocation type used by a UEFI Boot Service Driver to allocate pool memory. (Free after exit)
    BootServicesData,
    // The code portions of a loaded UEFI Runtime Driver
    RuntimeServicesCode,
    // The data portions of a loaded UEFI Runtime Driver and the default data allocation type used by a UEFI Runtime Driver to allocate pool memory.
    RuntimeServicesData,
    // Free (unallocated) memory
    Conventional,
    // Memory in which errors have been detected
    Unusable,
    // Memory that holds the ACPI tables.
    ACPIReclaim,
    // Address space reserved for use by the firmware.
    ACPINonVolatile,
    // Used by system firmware to request that a memory-mapped IO region be mapped by the OS to a virtual address so it can be accessed by EFI runtime services
    MMIO,
    // System memory-mapped IO region that is used to translate memory cycles to IO cycles by the processor.
    MMIOPortSpace,
    // Address space reserved by the firmware for code that is part of the processor
    PALCode,
    // A memory region that operates as EfiConventionalMemory. However, it happens to also support byte-addressable non-volatility.
    PersistentMemory,
    // A memory region that represents unaccepted memory, that must be accepted by the boot target before it can be used. Unless otherwise noted, all other EFI memory types are accepted. For platforms that support unaccepted memory, all unaccepted valid memory will be reported as unaccepted in the memory map. Unreported physical address ranges must be treated as not-present memory.
    Unaccepted,
}

pub fn get_memory(
    bs: &BootServices,
) -> Result<
    (
        usize,
        impl ExactSizeIterator + Iterator<Item = &MemoryDescriptor> + Clone,
    ),
    usize,
> {
    let (mmap_size, entry_size) = bs.memory_map_size()?;
    let max_size = mmap_size + 8 * entry_size;
    let mmap_storage = {
        let ptr = bs.allocate_pool(MemoryType::LoaderData, max_size)?;
        unsafe { slice::from_raw_parts_mut(ptr, max_size) }
    };
    bs.get_memory_map(mmap_storage)
}
