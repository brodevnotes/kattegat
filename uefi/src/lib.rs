#![no_std]
use core::{ffi::c_void, fmt::Write};

use efi::{
    find_rsdp, get_memory,
    protocols::{
        get_console_control, get_gop, get_uga, gop::GraphicsOutputProtocol, BltOperation, BltPixel,
        PixelFormat, EFI_GRAPHICS_OUTPUT_PROTOCOL,
    },
    BootServices, Guid, HandleBuffer, MemoryDescriptor, RSDPType,
};
pub mod efi;
pub use efi::{Handle, SystemTable};

#[derive(Debug)]
pub struct FrameBufferInfo {
    pub framebuffer_address: *mut c_void,
    pub framebuffer_size: usize,
    pub height: usize,
    pub width: usize,
    pub pixel_format: PixelFormat,
    pub stride: usize,
}

pub fn get_uefi_version(system_table: *mut SystemTable) -> (u16, u16) {
    unsafe {
        let uefi_revision = (*system_table).header().revision;
        let major = (uefi_revision >> 16) as u16;
        let minor = (uefi_revision & 0xffff) as u16;
        (major, minor)
    }
}

pub fn print_protocols(writer: &mut impl Write, boot_services: &BootServices, handle: Handle) {
    unsafe {
        if let Ok(available_protocols) = boot_services.protocols_per_handle(handle) {
            for protocol in available_protocols {
                let _ = writer.write_fmt(format_args!("image: {:?}", **protocol));
                let _ = writer.write_str("\r\n");
            }
        }
    }
}

pub fn print_console_control(
    writer: &mut impl Write,
    boot_services: &BootServices,
    handle: Handle,
) {
    match get_console_control(boot_services, handle) {
        Ok(control) => {
            let _ = writer.write_str("Console Control: ");
            let _ = writer.write_fmt(format_args!("{:?}", control.get_mode().0));
            let _ = writer.write_str(", ");
            let _ = writer.write_fmt(format_args!("{:?}", control.get_mode().1));
            let _ = writer.write_str(", ");
            let _ = writer.write_fmt(format_args!("{:?}", control.get_mode().2));
            let _ = writer.write_str("\r\n");
        }
        Err(status) => {
            let _ = writer.write_str("Console Control Error: ");
            let _ = writer.write_fmt(format_args!("{:?}", status));
            let _ = writer.write_str("\r\n");
        }
    }
}

pub fn get_supported_graphics<'boot>(
    writer: &'boot mut impl Write,
    boot_services: &'boot BootServices,
    debug: bool,
) -> Result<Handle, usize> {
    let buffer = &boot_services
        .locate_handle_buffer(efi::SearchType::ByProtocol(&EFI_GRAPHICS_OUTPUT_PROTOCOL))?;

    if debug {
        let _ = writer.write_str("Supported Graphics handlers: \r\n");
    }

    let mut working_handle = None;
    for handle in buffer.handles() {
        if debug {
            let _ = writer.write_fmt(format_args!("{:?} ", handle));
        }
        working_handle = Some(*handle);
    }

    if debug {
        let _ = writer.write_str("\r\n");
    }

    Ok(working_handle.expect("No graphics support found"))
}

/// Only Graphics Output Protocol supported (GOP)
pub fn get_graphics_supported(
    boot_services: &BootServices,
    handle: Handle,
) -> Result<&mut GraphicsOutputProtocol, usize> {
    match get_gop(&boot_services, handle) {
        Ok(gop) => {
            return Ok(gop);
        }

        Err(status) => {
            match get_uga(&boot_services, handle) {
                Ok(_uga) => {
                    // If UGA from EFI should be supported, framebuffer info is usually found on the PCI bus
                }
                Err(status) => {
                    return Err(status);
                }
            }
            return Err(status);
        }
    }
}

pub fn get_framebuffer_info(
    graphics_supported: &mut GraphicsOutputProtocol,
) -> Option<FrameBufferInfo> {
    let (framebuffer_address, framebuffer_size) = graphics_supported.frame_buffer();
    let mode_info = graphics_supported.mode_info();
    let pixel_format = mode_info.pixel_format();
    let (width, height) = mode_info.resolution();
    let stride = mode_info.stride();

    Some(FrameBufferInfo {
        framebuffer_address,
        framebuffer_size,
        height,
        width,
        pixel_format,
        stride,
    })
}

pub fn print_modes(writer: &mut impl Write, graphics_support: &mut GraphicsOutputProtocol) {
    for mode in graphics_support.modes() {
        let _ = writer.write_fmt(format_args!("{:?}", mode.info().resolution()));
        let _ = writer.write_str("\r\n");
    }
}

pub fn set_mode_and_clear_screen(
    graphics_support: &mut GraphicsOutputProtocol,
    wanted_resolution: Option<(usize, usize)>,
) -> Result<(), usize> {
    let graphics_mode = match wanted_resolution {
        Some(res) => match graphics_support
            .modes()
            .find(|mode| mode.info().resolution() == res)
        {
            Some(mode) => mode,
            None => return Err(0),
        },
        None => graphics_support
            .modes()
            .reduce(|chosen, current| {
                if current.info().resolution().0 > chosen.info().resolution().0 {
                    return current;
                } else {
                    return chosen;
                }
            })
            .unwrap(),
    };

    let status = graphics_support.set_mode(&graphics_mode);
    if status != 0 {
        return Err(status);
    }

    let op = BltOperation::VideoFill {
        color: BltPixel::new(2, 81, 170),
        destination: (0, 0),
        dimensions: graphics_mode.info().resolution(),
    };

    let status = graphics_support.blt(op);
    if status != 0 {
        return Err(status);
    }

    Ok(())
}

pub fn get_rsdp(system_table: &SystemTable) -> (RSDPType, *const c_void) {
    find_rsdp(system_table.acpi_tables())
}

pub fn get_memory_map(
    boot_services: &BootServices,
) -> Result<
    (
        usize,
        impl ExactSizeIterator + Iterator<Item = &MemoryDescriptor> + Clone,
    ),
    usize,
> {
    get_memory(&boot_services)
}

pub fn exit_uefi(
    boot_services: &BootServices,
    image_handle: Handle,
    memory_map_key: usize,
) -> Result<(), usize> {
    boot_services.exit_boot_services(image_handle, memory_map_key)
}
