#![no_std]
#![no_main]

use boot::BootInfo;
use uefi::{Handle, SystemTable};

#[cfg(target_arch = "x86_64")]
#[no_mangle]
extern "C" fn efi_main(image_handle: Handle, system_table: *mut SystemTable) -> usize {
    match boot::uefi::uefi_boot(image_handle, system_table) {
        Ok(boot_info) => kernel_start(boot_info),
        Err(status) => return status,
    }
}
pub fn kernel_start(_boot_info: BootInfo) -> ! {
    loop {}
}
