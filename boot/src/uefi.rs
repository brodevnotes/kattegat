use crate::{BootInfo, MemorySpace};
use core::fmt::Write;
use uefi::{
    efi::{protocols::PixelFormat, MemoryType},
    Handle, SystemTable,
};

pub fn uefi_boot(image_handle: Handle, system_table: *mut SystemTable) -> Result<BootInfo, usize> {
    unsafe {
        let con_out = (*system_table).console_out();
        con_out.reset(true);

        let mut boot_info = BootInfo::new();
        boot_info.init(con_out);

        let _ = con_out.write_str("UEFI Revision ");
        let (major, minor) = uefi::get_uefi_version(system_table);
        let _ = con_out.write_fmt(format_args!("{}.{}", major, minor));
        let _ = con_out.write_str("\r\n");

        let boot_services = (*system_table).boot_services();

        // For debugging purposes
        //uefi::print_protocols(con_out, boot_services, image_handle);
        //uefi::print_protocols(con_out, boot_services, console_out_handle);
        //uefi::print_console_control(con_out, boot_services, console_out_handle);
        let handle = uefi::get_supported_graphics(con_out, boot_services, false)?;

        match uefi::get_graphics_supported(boot_services, handle) {
            Ok(gop) => {
                if let Some(info) = uefi::get_framebuffer_info(gop) {
                    let pixel_format = match info.pixel_format {
                        PixelFormat::BlueGreenRedReserved8BitPerColor => crate::PixelFormat::BGR,

                        PixelFormat::RedGreenBlueReserved8BitPerColor => crate::PixelFormat::RGB,
                        PixelFormat::PixelBitMask => crate::PixelFormat::Bitmask,
                        _ => crate::PixelFormat::RGB,
                    };
                    boot_info.with_framebuffer(
                        info.framebuffer_address,
                        info.framebuffer_size,
                        (info.height, info.width),
                        pixel_format,
                        info.stride,
                    );
                }

                // For debugging purposes
                //uefi::print_modes(con_out, gop);

                //uefi::set_mode_and_clear_screen(gop, Some((800, 600)))?;
                uefi::set_mode_and_clear_screen(gop, None);
            }
            Err(_status) => {
                let _ = con_out.write_str("No supported framebuffer found.\r\n");
            }
        }

        let rsdp_info = uefi::get_rsdp(&*system_table);
        let _ = con_out.write_str("RSDP: ");
        let _ = con_out.write_fmt(format_args!("{:?}", rsdp_info));
        let _ = con_out.write_str("\r\n");

        boot_info.with_acpi_rsdp(rsdp_info.1);

        let (memory_map_key, memory_map) = uefi::get_memory_map(&boot_services)?;

        let free_memory = memory_map.into_iter().filter(|m| match m.memory_type {
            MemoryType::Conventional => true,
            _ => false,
        });

        let mut biggest_memory: Option<MemorySpace> = None;
        for mem in free_memory {
            let size = (mem.number_of_pages * 4096) as usize;

            if biggest_memory.is_none() || biggest_memory.unwrap().size < size {
                biggest_memory = Some(MemorySpace {
                    address: mem.physical_start,
                    size,
                });
            }
        }

        let free_address = biggest_memory.unwrap();

        let _ = con_out.write_str("Free memory: ");
        let _ = con_out.write_fmt(format_args!("{:?}", free_address));
        let _ = con_out.write_str("\r\n");

        boot_info.with_free_memory(free_address);

        let mut tries = 4;
        let mut mem_key = memory_map_key;

        // By UEFI specification exiting UEFI boot services should be tried until correct memory key is found.
        // We try 4 times more after the first retrieval, 5 in total.
        for _ in 0..3 {
            match uefi::exit_uefi(boot_services, image_handle, mem_key) {
                Ok(()) => {
                    break;
                }
                Err(status) => {
                    let _ = con_out.write_str("UEFI exit: EFI_INVALID_PARAMETER, trying ");
                    let _ = con_out.write_fmt(format_args!("{:?}", 6 - tries));
                    let _ = con_out.write_str(" out of 5");
                    let _ = con_out.write_str("\r\n");
                    tries -= 1;
                    if tries <= 0 {
                        let _ = con_out.write_str("UEFI exit: EFI_INVALID_PARAMETER");
                        let _ = con_out.write_fmt(format_args!("{:X}", status));
                        break;
                    }
                    (mem_key, _) = uefi::get_memory_map(&boot_services)?;
                }
            };
        }

        Ok(boot_info)
    }
}
