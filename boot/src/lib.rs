#![no_std]
use core::{ffi::c_void, fmt::Write};

pub mod uefi;

#[derive(Debug)]
pub enum PixelFormat {
    RGB,
    BGR,
    Bitmask,
}

#[derive(Debug)]
pub struct FrameBufferInfo {
    pub framebuffer_address: *mut c_void,
    pub framebuffer_size: usize,
    pub height: usize,
    pub width: usize,
    pub pixel_format: PixelFormat,
    pub stride: usize,
}

#[derive(Debug)]
pub struct BootInfo {
    framebuffer_info: Option<FrameBufferInfo>,
    acpi_rsdp: Option<*const c_void>,
    free_memory: Option<MemorySpace>,
}

#[derive(Copy, Clone)]
pub struct MemorySpace {
    pub address: u64,
    pub size: usize,
}

impl core::fmt::Debug for MemorySpace {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let _ = f.write_str("address: ");
        let _ = f.write_fmt(format_args!("{:#X}", &self.address));
        let _ = f.write_str(", size: ");
        let (num, postfix) = format_bytes(self.size);
        let _ = f.write_fmt(format_args!("{:.2}", num));
        let _ = f.write_str(postfix);
        Ok(())
    }
}

fn format_bytes(bytes: usize) -> (f64, &'static str) {
    let float_bytes = bytes as f64;
    if bytes >= 1073741824 {
        return (float_bytes / 1073741824.0, "GB");
    }
    if bytes >= 1048576 {
        return (float_bytes / 1048576.0, "MB");
    }
    if bytes >= 1024 {
        return (float_bytes / 1024.0, "kB");
    }

    (float_bytes, "B")
}

impl BootInfo {
    pub fn new() -> BootInfo {
        BootInfo {
            framebuffer_info: None,
            acpi_rsdp: None,
            free_memory: None,
        }
    }

    pub fn init(&self, writer: &mut impl Write) {
        let _ = writer.write_str("Booting system\r\n");
    }

    pub fn print_info(&self, writer: &mut impl Write) {
        if let Some(info) = &self.framebuffer_info {
            let _ = writer.write_str("Framebuffer\r\n");
            let _ = writer.write_fmt(format_args!("{:?}", info));
            let _ = writer.write_str("\r\n");
        }
    }

    pub fn with_framebuffer(
        &mut self,
        address: *mut c_void,
        size: usize,
        dimensions: (usize, usize),
        pixel_format: PixelFormat,
        stride: usize,
    ) {
        self.framebuffer_info = Some(FrameBufferInfo {
            framebuffer_address: address,
            framebuffer_size: size,
            height: dimensions.0,
            width: dimensions.1,
            pixel_format,
            stride,
        })
    }

    pub fn with_acpi_rsdp(&mut self, rsdp: *const c_void) {
        self.acpi_rsdp = Some(rsdp);
    }

    pub fn with_free_memory(&mut self, memory_address: MemorySpace) {
        self.free_memory = Some(memory_address);
    }
}
